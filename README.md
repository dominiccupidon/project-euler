# Project Euler Solutions

## About
[Project Euler](https://projecteuler.net/about) is a series problems that aim to challenge one's mathematical and computational skills. I believe that by going through this series I will build my problem skills, knowledge of Mathematics and proficiency with Python. 

**Note**: Although all problems work with specific values, I decided, where possible, to implement solutions that work with user-inputted values.

## Solutions
The team behind Project Euler kindly requests that discussions of solutions be limited to the [first 100 problems](https://projecteuler.net/about#publish). I will respect their wishes. I understand that publicly posting full solutions can be seen as undermining the goal of Project Euler; this is not my intention. My desire is that viewers of the repository will focus on my knowledge of Mathematics, skills in Python and approaches to problem solving.

All solutions have been verified to be correct using Project Euler's answer checker.

## Running the programme
Run `python main.py` (Windows) or `python3 main.py` (Mac and Linux). The script will give further instructions.