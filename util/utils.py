# File that contains functions that will be useful to solving multiple
# Project Euler problems

# Determines whether a number is prime using the 6k+-1 optimisation
# Source: Wikipedia
# This function has been included as my original method for prime number checking
# was woefully inefficient. After researching into the methods to check for primes, 
# I concluded this method was efficient enough for my needs
def is_prime(n: int):
    if n <= 3:
        return n > 1
    if n % 2 == 0 or n % 3 == 0:
        return False
    i = 5
    while i ** 2 <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i += 6
    return True

# Retrieves the first prime after n. n > 1
def next_prime(curr_prime: int):
    # The selected range makes use of Bertrand's postulate, which states:
    # for any integer n > 3, there always exists at least one prime number p with
    # n < p < 2n - 2
    # Modifications have made to range to allow for values n > 1 to return primes
    for i in range(curr_prime + 1, 2 * curr_prime):
        if (is_prime(i)):
            return i

# Splits number into its individual digits and adds each digit to 
# a list
def split_num(x, digits):
    if x < 10:
        digits.append(x)
    else:
        digits.append(x % 10)
        split_num(int(x/10), digits)

# Calculates the gcd of two numbers using the Euclidean algorithm
def gcd(x, y):
    if (x == 0):
        return y
    if (y == 0):
        return x
    
    if (x >= y):
        return gcd(x % y, y)
    if (y >= x):
        return gcd(x, y % x)
