import solutions.p1 as p1
import solutions.p2 as p2
import solutions.p3 as p3
import solutions.p4 as p4
import solutions.p5 as p5
import solutions.p6 as p6
import solutions.p7 as p7
import solutions.p8 as p8
import solutions.p9 as p9
import solutions.p10 as p10
import solutions.p11 as p11

PROBLEMS_SOLVED = 11
prompts = [
    "multiples of 3 and 5 less than n can be summed",
    "even numbers in the Fibonacci sequence can be summed",
    "n's largest prime factor can be found",
    None,
    "the smallest integer that can be evenly divided by 1 through n can be found",
    """the difference between the sum of the squares and the square of the 
    sum of the first n natural numbers can be calculated""",
    "nth prime number will be found",
    None, 
    None,
    "prime numbers less than n will be summed",
    None
]

# Allows users to select which problem solution they would like to see
def choose_solution():
    try:
        prob = int(input(f"Enter a number from 1 to {PROBLEMS_SOLVED}: ")) 
        if (prob > PROBLEMS_SOLVED):
            raise IndexError(f"Problem {prob} has not been solved")
    except IndexError as e:
        print(e)
    except ValueError:
        print("Incorrect data type entered")
    else:
        select_question_bounds(prob)
        
# All questions have specific parameters to them. For some questions, it is easy to create
# solutions that work with all possible parameters. For those questions, this method 
def select_question_bounds(prob: int):
    use_default_values =  True 
    n = None
    try:
        if (prompts[prob - 1] != None):
            res = input("Do you want to use the default bounds for the solution? "
                + "Y for yes and N for no: ")
            if (res.find('n') != -1 or res.find('N') != -1):
                use_default_values = False  
            if (not use_default_values):
                n = int(input(f"Choose the value of n, for which {prompts[prob - 1]}: "))
    except ValueError:
        print("Incorrect data type entered")
    else:
        start_question(prob, n)

def start_question(prob:int, n):
    if prob == 1:
        print(f"Solution to problem 1: {p1.solution(n)}")
    elif prob == 2:
        print(f"Solution to problem 2: {p2.solution(n)}")
    elif prob == 3:
        print(f"Solution to problem 3: {p3.solution(n)}")
    elif prob == 4:
        print(f"Solution to problem 4: {p4.solution()}")
    elif prob == 5:
        print(f"Solution to problem 5: {p5.solution(n)}")
    elif prob == 6:
        print(f"Solution to problem 6: {p6.solution(n)}")
    elif prob == 7:
        print(f"Solution to problem 7: {p7.solution(n)}")
    elif prob == 8:
        print(f"Solution to problem 8: {p8.solution()}")
    elif prob == 9:
        print(f"Solution to problem 9: {p9.solution()}")
    elif prob == 10:
        print(f"Solution to problem 10: {p10.solution(n)}")
    elif prob == 11:
        print(f"Solution to problem 11: {p11.solution()}")

def main():
    # Allows users to view more than solution during a single run of the programme
    cont = True
    while cont:
        choose_solution()
        res = input("Do you want to test another solution? Y for yes and N for no: ")
        if (res.find("n") != -1 or res.find("N") != -1):
            cont = False

    
if __name__ == "__main__":
    main()