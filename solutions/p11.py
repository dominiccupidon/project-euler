# Solution for problem 11 of Project Euler. 
# Link: https://projecteuler.net/problem=11

# 20 by 20 2D array of numbers
grid = [
    [8, 2, 22, 97, 38, 15, 0, 40, 0, 75, 4, 5, 7, 78, 52, 12, 50, 77, 91, 8],
    [49, 49, 99, 40, 17, 81, 18, 57, 60, 87, 17, 40, 98, 43, 69, 48, 4, 56, 62, 0],
    [81, 49, 31, 73, 55, 79, 14, 29, 93, 71, 40, 67, 53, 88, 30, 3, 49, 13, 36, 65],
    [52, 70, 95, 23, 4, 60, 11, 42, 69, 24, 68, 56, 1, 32, 56, 71, 37, 2, 36, 91],
    [22, 31, 16, 71, 51, 67, 63, 89, 41, 92, 36, 54, 22, 40, 40, 28, 66, 33, 13, 80],
    [24, 47, 32, 60, 99, 3, 45, 2, 44, 75, 33, 53, 78, 36, 84, 20, 35, 17, 12, 50],
    [32, 98, 81, 28, 64, 23, 67, 10, 26, 38, 40, 67, 59, 54, 70, 66, 18, 38, 64, 70],
    [67, 26, 20, 68, 2, 62, 12, 20, 95, 63, 94, 39, 63, 8, 40, 91, 66, 49, 94, 21],
    [24, 55, 58, 5, 66, 73, 99, 26, 97, 17, 78, 78, 96, 83, 14, 88, 34, 89, 63, 72],
    [21, 36, 23, 9, 75, 0, 76, 44, 20, 45, 35, 14, 0, 61, 33, 97, 34, 31, 33, 95],
    [78, 17, 53, 28, 22, 75, 31, 67, 15, 94, 3, 80, 4, 62, 16, 14, 9, 53, 56, 92],
    [16, 39, 5, 42, 96, 35, 31, 47, 55, 58, 88, 24, 0, 17, 54, 24, 36, 29, 85, 57],
    [86, 56, 0, 48, 35, 71, 89, 7, 5, 44, 44, 37, 44, 60, 21, 58, 51, 54, 17, 58],
    [19, 80, 81, 68, 5, 94, 47, 69, 28, 73, 92, 13, 86, 52, 17, 77, 4, 89, 55, 40],
    [4, 52, 8, 83, 97, 35, 99, 16, 7, 97, 57, 32, 16, 26, 26, 79, 33, 27, 98, 66],
    [88, 36, 68, 87, 57, 62, 20, 72, 3, 46, 33, 67, 46, 55, 12, 32, 63, 93, 53, 69],
    [4, 42, 16, 73, 38, 25, 39, 11, 24, 94, 72, 18, 8, 46, 29, 32, 40, 62, 76, 36],
    [20, 69, 36, 41, 72, 30, 23, 88, 34, 62, 99, 69, 82, 67, 59, 85, 74, 4, 36, 16],
    [20, 73, 35, 29, 78, 31, 90, 1, 74, 31, 49, 71, 48, 86, 81, 16, 23, 57, 5, 54],
    [1, 70, 54, 71, 83, 51, 54, 69, 16, 92, 33, 48, 61, 43, 52, 1, 89, 19, 67, 48]
]

# Since the problem involved connecting "points" within a certain "space" and then comparing 
# the "weights" of those connections, I believed that arranging the numbers as a graph would 
# the best way to create a solution. I chose a dictionary to represent the graph as it was 
# the first data structure that came up when I was refreshing my knowledge of graphs
class Graph:
    def __init__(self, grid: list):
        self.grid = grid
        self.graph_dict = {}
        self.__construct_graph()

    def __construct_graph(self):
        for x in range(len(self.grid)):
            for y in range(len(self.grid[x])):
                # Hashing functioh to create unique keys based on the positon of the number within
                # the 2D array
                key = 31 * x + (17 * y) % 59

                # I defined a connection or edge within this graph as being a series of four adjacent
                # numbers, no matter the direction. I quickly realised that the position of the number
                # in the 2D space determined how many other numbers it would be connected to. I then 
                # divided the space based on the number of connections. For a given value grid[x][y], if:
                # * x and y are members of [0 - 2] U [17 - 19], the number has 3 connections
                # * x is a member of [0 - 2] and y is a member of [3 - 16], the number has 5 connections
                # * x is a member of [3 - 16] and y is a member of [0 - 2], the number has 5 connections
                # * x and y are members of [3 - 16], the number has 8 connections
                #
                # If the number of connections < 8, the positon of grid[x][y] also determines the direction 
                # of its connections, e.g. if x < 3 and y < 3, grid[x][y] has connections going to its east,
                # southeast and south. I used these two facts to determine the number of ways edges could be
                # retrieved for a given value grid[x][y].
                #
                # To make things easier, I assigned each cardinal direction a letter: "A" for north, "B"
                # for northeast and so on.
                if ((x >= 3) and (x <= 16) and (y >= 3) and (y <= 16)):
                    self.graph_dict[f"{key}"] = self.__edge_products(x, y, 
                        ["A", "B", "C", "D", "E", "F", "G", "H"])
                elif ((x >= 3) and (x <= 16) and (y < 3)):
                    self.graph_dict[f"{key}"] = self.__edge_products(x, y, 
                        ["G", "F", "E", "D", "C"])
                elif ((x >= 3) and (x <= 16) and (y > 16)):
                    self.graph_dict[f"{key}"] = self.__edge_products(x, y, 
                        ["G", "H", "A", "B", "C"])
                elif ((x < 3) and (y >= 3) and (y <= 16)):
                    self.graph_dict[f"{key}"] = self.__edge_products(x, y,
                        ["A", "B", "C", "D", "E"])
                elif ((x > 16) and (y >= 3) and (y <= 16)):
                    self.graph_dict[f"{key}"] = self.__edge_products(x, y,
                        ["A", "H", "G", "F", "E"])
                elif ((x < 3) and (y < 3)):
                    self.graph_dict[f"{key}"] = self.__edge_products(x, y,
                        ["C", "D", "E"])
                elif ((x < 3) and (y > 16)):
                    self.graph_dict[f"{key}"] = self.__edge_products(x, y,
                        ["A", "B", "C"])
                elif ((x > 16) and (y < 3)):
                    self.graph_dict[f"{key}"] = self.__edge_products(x, y, 
                        ["G", "F", "E"])
                elif ((x > 16) and (y < 16)):
                    self.graph_dict[f"{key}"] = self.__edge_products(x, y, 
                        ["G", "H", "A"])
     
    # edge_types: A list of all valid edge directions, for a postion (x, y) 
    #
    # Calculates the product of the four adjacent numbers within a particular
    # edge direction
    def __edge_products(self, x, y, edge_types: list):
        edges = list()
        for type in edge_types:
            product = 0
            # up
            if type == "A":
                product = self.grid[x][y] * self.grid[x][y - 1] * \
                    self.grid[x][y - 2] * self.grid[x][y - 3]
            # diagonal: up and to the right
            elif type == "B":
                product = self.grid[x][y] * self.grid[x + 1][y - 1] * \
                    self.grid[x + 2][y - 2] * self.grid[x + 3][y - 3]
            # right
            elif type == "C":
                product = self.grid[x][y] * self.grid[x + 1][y] * \
                    self.grid[x + 2][y] * self.grid[x + 3][y]
            # diagonal: down and to the right
            elif type == "D":
                product = self.grid[x][y] * self.grid[x + 1][y + 1] * \
                    self.grid[x + 2][y + 2] * self.grid[x + 3][y + 3]
            # down
            elif type == "E":
                product = self.grid[x][y] * self.grid[x][y + 1] * \
                    self.grid[x][y + 2] * self.grid[x][y + 3]
            # diagonal: down and to the left
            elif type == "F":
                product = self.grid[x][y] * self.grid[x - 1][y + 1] * \
                    self.grid[x - 2][y + 2] * self.grid[x - 3][y + 3]
            # left
            elif type == "G":
                product = self.grid[x][y] * self.grid[x - 1][y] * \
                    self.grid[x - 2][y] * self.grid[x - 3][y]
            # diagonal: up and to the left
            elif type == "H":
                product = self.grid[x][y] * self.grid[x - 1][y - 1] * \
                    self.grid[x - 2][y - 2] * self.grid[x - 3][y - 3]

            edges.append(product)
        return edges

def solution():
    graph = Graph(grid)
    max_product = 0
    for val in graph.graph_dict.values():
        for product in val:
            if product > max_product:
                max_product = product

    return max_product
