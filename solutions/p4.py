import sys
sys.path.insert(0, "../util")
import util.utils as utils

# Solution for problem 4 of Project Euler. 
# Link: https://projecteuler.net/problem=4
def solution():
    # Selects palindromes from the integers in range [100^2, 999^2] 
    # Determines the largest palindrome
    max_val = 0
    for i in range(100, 1000):
        for j in range(100, 1000):
            k = i * j
            if (__is_palindrome(k) and k > max_val):
                   max_val = k
    
    return max_val

# Determines if a digit is a palindrome
def __is_palindrome(x):
    digits = list()
    utils.split_num(x, digits) 
    for i in range(int(len(digits) / 2)):
        if (digits[i] != digits[len(digits) - (i + 1)]):
            return False
    return True
