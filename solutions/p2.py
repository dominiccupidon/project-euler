# Solution for problem 2 of Project Euler. 
# Link: https://projecteuler.net/problem=2
def solution(max_int):
    max_int = 4_000_000 if max_int is None else max_int
    # First 2 digits of the Fibonacci sequence
    i, j = 1, 2
    evens = list()
    evens.append(j)
    while i + j < max_int:
        # Tests whether the next entry will be even
        if ((i + j) % 2 == 0):
            evens.append(i + j)
        # Progresses through the sequence
        i, j = j, i + j
    return sum(evens)