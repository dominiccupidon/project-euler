# Solution for problem 1 of Project Euler. 
# Link: https://projecteuler.net/problem=1
def solution(max_int):
    max_int = 1000 if max_int is None else max_int
    # Creates a range of elements from 0 to max_int - 1 
    # Filters the range for elements that are either multiples of 3 or 5.
    select_ints = filter(lambda i: i % 3 == 0 or i % 5 == 0, range(max_int))
    # Sums the elements in the filter
    return sum(select_ints)

