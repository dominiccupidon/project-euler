import sys
sys.path.insert(0, "../util")
import util.utils as utils

# Solution for problem 9 of Project Euler. 
# Link: https://projecteuler.net/problem=9

# This solution makes use of Euclid's formula for calculating Pythagorean triples.
# If a^2 + b^2 = c^2, then: a = k(m^2 - n^2), b = k(2mn) c = k(m^2 + n^2), where:
# * m, n and k are positive integers, 
# * m > n, 
# * gcd(m, n) = 1 and 
# * m and n are not both odd
# By substituting the equations for a, b and c into a + b + c = 1000, the equation 
# n = 500/mk - m is obtained. The algorithm below uses a brute force method to solve
# for n, m and k
def solution():
    factors = [1, 2, 4, 5, 10, 20, 25, 50, 100, 125, 250, 500]
    solutions_of_mnk = list()
    triple = list()
    for index, value in enumerate(factors):
        if index > 0:
            # Due to the list's order all factors of value lie in indexes before it. 
            # Since value is a factor of 500, value's factors are possible solutions for m and k.
            possible_vals_of_m = filter(lambda x, value=value: value % x == 0, factors[0:index])
            for m in possible_vals_of_m:
                n = (500/value - m)
                # Checks to ensure the conditions on m, n and k are met
                if (m > n and n  > 0 and not (m % 2 == n % 2 == 1) and utils.gcd(m, n) == 1):
                    solutions_of_mnk.append(m)
                    solutions_of_mnk.append(n)
                    solutions_of_mnk.append(value/m)

    # Calculates the Pythagorean triple and return their product
    a = solutions_of_mnk[2] * (solutions_of_mnk[0] ** 2 - solutions_of_mnk[1] ** 2)
    b = solutions_of_mnk[2] * (2 * solutions_of_mnk[0] * solutions_of_mnk[1])
    c = solutions_of_mnk[2] * (solutions_of_mnk[0] ** 2 + solutions_of_mnk[1] ** 2)

    return a * b * c