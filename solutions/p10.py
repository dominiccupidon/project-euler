import sys
sys.path.insert(0, "../util")
import util.utils as utils

# Solution for problem 10 of Project Euler. 
# Link: https://projecteuler.net/problem=10
def solution(max_int):
    max_int = 2_000_000 if max_int is None else max_int
    p = 2
    total = 0
    while p < max_int:
        total += p
        p = utils.next_prime(p)
    
    return total