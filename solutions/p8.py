import sys
import functools
sys.path.insert(0, "../util")
import util.utils as utils

# Solution for problem 8 of Project Euler. 
# Link: https://projecteuler.net/problem=8

# The 1000 digit number NUM is stored a string because manipulating
# the characters of a string is easier than splitting a number a part
NUM = """73167176531330624919225119674426574742355349194934
96983520312774506326239578318016984801869478851843
85861560789112949495459501737958331952853208805511
12540698747158523863050715693290963295227443043557
66896648950445244523161731856403098711121722383113
62229893423380308135336276614282806444486645238749
30358907296290491560440772390713810515859307960866
70172427121883998797908792274921901699720888093776
65727333001053367881220235421809751254540594752243
52584907711670556013604839586446706324415722155397
53697817977846174064955149290862569321978468622482
83972241375657056057490261407972968652414535100474
82166370484403199890008895243450658541227588666881
16427171479924442928230863465674813919123162824586
17866458359124566529476545682848912883142607690042
24219022671055626321111109370544217506941658960408
07198403850962455444362981230987879927244284909188
84580156166097919133875499200524063689912560717606
05886116467109405077541002256983155200055935729725
71636269561882670428252483600823257530420752963450""".replace("\n", "")

def solution():
    i = 0
    max_val = 0;
    # Traverses string in normal order
    while i + 13 < len(NUM):
        sub_str = NUM[i:i+13]
        (max_val, j) = __calc_max_val(sub_str, max_val, i)
        # The position of i is readjusted pased on the tests performed in
        # __calc_max_val()
        if (i != j):
            i = j
        else: 
            i += 1
        del sub_str

    # Traverses string in reverse order
    while (len(NUM) - (i + 13)) >= 0:
        sub_str = NUM[(len(NUM) - (i + 13)):(len(NUM) - (i + 1))]
        (max_val, j) = __calc_max_val(sub_str, max_val, i)
        if (i != j):
            i = j
        else: 
            i += 1
        del sub_str
    return max_val

def __calc_max_val(sub_str: str, max_val: int, i:int):
    # Determines if there is an instance of "0" in the substring
    pos_of_zero = sub_str.find("0")
    # If so, increments the postion of i to the index after (going in normal order)
    # or before (going in reverse order) the instance. Otherwise, the max value is
    # calculated
    if (pos_of_zero != -1):
        i += (pos_of_zero + 1)
    else:
        num = int(sub_str)
        digits = list()
        utils.split_num(num, digits)
        val = functools.reduce(lambda x, y: x * y, digits)
        if (val > max_val):
            max_val = val
    return (max_val, i)
            
