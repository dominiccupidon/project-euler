# Solution for problem 6 of Project Euler. 
# Link: https://projecteuler.net/problem=6
def solution(max_int):
    max_int = 100 if max_int is None else max_int
    square_of_sum = ((max_int * (max_int + 1)) / 2) ** 2
    sum_of_squares = sum(map(lambda x: x ** 2, range(1, max_int + 1)))
    return square_of_sum - sum_of_squares