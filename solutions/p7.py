import sys
sys.path.insert(0, "../util")
import util.utils as utils

# Solution for problem 7 of Project Euler. 
# Link: https://projecteuler.net/problem=7
def solution(pos):
    pos = 10_001 if pos is None else pos
    p = 2
    for i in range(pos - 1):
        p = utils.next_prime(p)
    return p