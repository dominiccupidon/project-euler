import sys
sys.path.insert(0, "../util")
import util.utils as utils

# Solution for problem 3 of Project Euler. 
# Link: https://projecteuler.net/problem=3
def solution(number):
    number = 600_851_475_143 if number is None else number
    x = number
    p = 2

    if x == 1:
        return "No solution"

    # Uses prime factorisation to break down the value
    #  of max_int. It starts the factorisation using primes in 
    # acsending order, therefore the solution will be the value
    # of p when the loop ends.
    while x != 1:
        # Checks if p is currently a prime factor of x. If so
        # divide x, else move unto to next prime number.
        if (x % p == 0):
            x = x / p
        else:
            p = utils.next_prime(p)
    return p