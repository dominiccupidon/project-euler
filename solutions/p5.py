import sys
sys.path.insert(0, "../util")
import util.utils as utils

# Solution for problem 5 of Project Euler. 
# Link: https://projecteuler.net/problem=5
def solution(max_int):
    max_int = 20 if max_int is None else max_int
    p = 2
    res = 1
    # For each prime number < max_int, finds the highest power
    # of the prime number < max_int. The final result is multiplied 
    # by this number
    while p <= max_int:
        x = max_int
        power = 1
        while int(x/(p ** power)) >= 1:
            power += 1
        res  *= (p ** (power - 1))
        p = utils.next_prime(p)
    return res